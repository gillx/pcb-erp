package com.ruoyi.erp.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpOrder;
import com.ruoyi.erp.service.IErpOrderService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpOrder")
public class ErpOrderController extends BaseController
{
    private String prefix = "erp/erpOrder";

    @Autowired
    private IErpOrderService erpOrderService;

    @RequiresPermissions("erp:erpOrder:view")
    @GetMapping()
    public String erpOrder()
    {
        return prefix + "/erpOrder";
    }

    /**
     * 查询订单列表
     */
    @RequiresPermissions("erp:erpOrder:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpOrder erpOrder)
    {
        startPage();
        List<ErpOrder> list = erpOrderService.selectErpOrderList(erpOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @RequiresPermissions("erp:erpOrder:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpOrder erpOrder)
    {
        List<ErpOrder> list = erpOrderService.selectErpOrderList(erpOrder);
        ExcelUtil<ErpOrder> util = new ExcelUtil<ErpOrder>(ErpOrder.class);
        return util.exportExcel(list, "erpOrder");
    }

    /**
     * 新增订单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单
     */
    @RequiresPermissions("erp:erpOrder:add")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpOrder erpOrder)
    {
        return toAjax(erpOrderService.insertErpOrder(erpOrder));
    }

    /**
     * 修改订单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpOrder erpOrder = erpOrderService.selectErpOrderById(id);
        mmap.put("erpOrder", erpOrder);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单
     */
    @RequiresPermissions("erp:erpOrder:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpOrder erpOrder)
    {
        return toAjax(erpOrderService.updateErpOrder(erpOrder));
    }

    /**
     * 删除订单
     */
    @RequiresPermissions("erp:erpOrder:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpOrderService.deleteErpOrderByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpOrder erpOrder = erpOrderService.selectErpOrderById(id);
        mmap.put("erpOrder", erpOrder);
        return prefix + "/detail";
    }

    @RequiresPermissions("erp:erpOrder:view")
    @GetMapping("/pageSelectOrder")
    public String pageSelectOrder() {
        return prefix + "/pageSelectOrder";
    }
}

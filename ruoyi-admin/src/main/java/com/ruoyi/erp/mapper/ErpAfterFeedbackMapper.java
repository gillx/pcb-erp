package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpAfterFeedback;

/**
 * 售后反馈Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpAfterFeedbackMapper 
{
    /**
     * 查询售后反馈
     * 
     * @param id 售后反馈ID
     * @return 售后反馈
     */
    public ErpAfterFeedback selectErpAfterFeedbackById(String id);

    /**
     * 查询售后反馈列表
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 售后反馈集合
     */
    public List<ErpAfterFeedback> selectErpAfterFeedbackList(ErpAfterFeedback erpAfterFeedback);

    /**
     * 新增售后反馈
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 结果
     */
    public int insertErpAfterFeedback(ErpAfterFeedback erpAfterFeedback);

    /**
     * 修改售后反馈
     * 
     * @param erpAfterFeedback 售后反馈
     * @return 结果
     */
    public int updateErpAfterFeedback(ErpAfterFeedback erpAfterFeedback);

    /**
     * 删除售后反馈
     * 
     * @param id 售后反馈ID
     * @return 结果
     */
    public int deleteErpAfterFeedbackById(String id);

    /**
     * 批量删除售后反馈
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpAfterFeedbackByIds(String[] ids);
}

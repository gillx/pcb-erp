package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpStockOutLogMapper;
import com.ruoyi.erp.domain.ErpStockOutLog;
import com.ruoyi.erp.service.IErpStockOutLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 出库日志Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpStockOutLogServiceImpl implements IErpStockOutLogService 
{
    @Autowired
    private ErpStockOutLogMapper erpStockOutLogMapper;

    /**
     * 查询出库日志
     * 
     * @param id 出库日志ID
     * @return 出库日志
     */
    @Override
    public ErpStockOutLog selectErpStockOutLogById(String id)
    {
        return erpStockOutLogMapper.selectErpStockOutLogById(id);
    }

    /**
     * 查询出库日志列表
     * 
     * @param erpStockOutLog 出库日志
     * @return 出库日志
     */
    @Override
    public List<ErpStockOutLog> selectErpStockOutLogList(ErpStockOutLog erpStockOutLog)
    {
        return erpStockOutLogMapper.selectErpStockOutLogList(erpStockOutLog);
    }

    /**
     * 新增出库日志
     * 
     * @param erpStockOutLog 出库日志
     * @return 结果
     */
    @Override
    public int insertErpStockOutLog(ErpStockOutLog erpStockOutLog)
    {
        erpStockOutLog.setId(IdUtils.fastSimpleUUID());
        erpStockOutLog.setCreateTime(DateUtils.getNowDate());
        return erpStockOutLogMapper.insertErpStockOutLog(erpStockOutLog);
    }

    /**
     * 修改出库日志
     * 
     * @param erpStockOutLog 出库日志
     * @return 结果
     */
    @Override
    public int updateErpStockOutLog(ErpStockOutLog erpStockOutLog)
    {
        erpStockOutLog.setUpdateTime(DateUtils.getNowDate());
        return erpStockOutLogMapper.updateErpStockOutLog(erpStockOutLog);
    }

    /**
     * 删除出库日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpStockOutLogByIds(String ids)
    {
        return erpStockOutLogMapper.deleteErpStockOutLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除出库日志信息
     * 
     * @param id 出库日志ID
     * @return 结果
     */
    @Override
    public int deleteErpStockOutLogById(String id)
    {
        return erpStockOutLogMapper.deleteErpStockOutLogById(id);
    }
}
